defmodule His.Sessions do
  alias His.Sessions.Auth
  import Plug.Conn, only: [get_session: 2, assign: 3, halt: 1]
  import Phoenix.Controller, only: [put_flash: 3, redirect: 2, render: 4]

  def login(email, password) do
    Auth.by_email_and_password(email, password)
  end

  defmodule Login do
    def init(_opts), do: nil

    def call(conn, _params) do
      user_id = get_session(conn, :user_id)
      assign(conn, :current_user, His.Accounts.get_user(user_id))
    end
  end

  defmodule Logged do
    def init(_opts), do: nil

    def call(conn, _params) do
      if conn.assigns[:current_user] do
        conn
      else
        conn
        |> put_flash(:error, "Требуется вход в систему.")
        |> redirect(to: His.Web.Router.Helpers.page_path(conn, :index))
        |> halt()
      end
    end
  end

  defmodule AdminOnly do
    def init(_opts), do: nil

    def call(conn, _params) do
      user = conn.assigns[:current_user]
      if user && user.position == "admin" do
        conn
      else
        conn
        |> render(His.Web.ErrorView, "404.html", [])
        |> halt()
      end
    end
  end
end

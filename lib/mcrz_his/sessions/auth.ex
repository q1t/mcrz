defmodule His.Sessions.Auth do
  alias His.Repo
  alias His.Accounts.User
  alias Comeonin.Bcrypt

  def by_email_and_password(email, password) do
    case Repo.get_by(User, email: email) do
      user = %User{} ->
        check_password(user, password)
      nil ->
        Bcrypt.dummy_checkpw()
        {:error, :not_found}
    end
  end

  defp check_password(user = %User{password_hash: pwd_hash}, pwd) do
    if Bcrypt.checkpw(pwd, pwd_hash) do
      {:ok, user}
    else
      {:error, :incorrect}
    end
  end
end

defmodule His.Accounts do
  import Ecto.{Query, Changeset}, warn: false
  alias His.Repo
  alias His.Accounts.{User, Auth}

  def register_user(attrs) do
    %User{}
    |> registration_changeset(attrs)
    |> check_password()
    |> put_password_hash()
    |> put_email_verification_key()
    |> Repo.insert()
  end

  def partial_register_user(attrs) do
    %User{}
    |> partial_registration_changeset(attrs)
    |> put_email_verification_key()
    |> put_password_reset_key()
    |> Repo.insert()
  end

  def get_user_by_email(email), do: Repo.get_by(User, email: email)
  def get_user(nil), do: nil
  def get_user(id), do: Repo.get(User, id)

  def init_password_reset(email) do
    with user = %User{} <- get_user_by_email(email),
         {:ok, user} <- set_password_reset_key(user) do
      His.Email.password_reset(user.email, user.password_reset_key)
      |> His.Mailer.deliver_later
      :ok
    else
      _ -> :error
    end
  end

  defp set_password_reset_key(user) do
    change(user, %{password_reset_key: Auth.gen_key()})
    |> Repo.update
  end

  defp put_password_reset_key(changeset) do
    put_change(changeset, :password_reset_key, Auth.gen_key())
  end

  def update_password(email, key, password) do
    with user = %User{} <- get_user_by_email(email),
         true <- verify_password_key(user, key),
         {:ok, user} <- set_password(user, password) do
      :ok
    else
      res when res in [false, nil] ->
        :error
      other ->
        other
    end
  end

  defp verify_password_key(%User{password_reset_key: key}, key), do: true
  defp verify_password_key(_, _), do: false

  defp set_password(user, password_params) do
    user
    |> new_password(password_params)
    |> put_change(:password_reset_key, nil)
    |> Repo.update()
  end

  def new_password(user \\ %User{}, attrs \\ %{}) do
    user
    |> new_password_changeset(attrs)
    |> check_password()
    |> put_password_hash()
  end

  def check_password(changeset) do
    changeset
    |> validate_length(:password, min: 6)
    |> validate_confirmation(:password, required: true)
  end


  def change_registration(%User{} = reg) do
    registration_changeset(reg, %{})
  end

  defp partial_registration_changeset(%User{} = reg, attrs) do
    reg
    |> cast(attrs, User.reg_fields())
    |> validate_required(User.reg_fields() -- [:middle_name, :password])
    |> validate_format(:email, ~r/@/)
    |> unique_constraint(:email)
  end

  defp registration_changeset(%User{} = reg, attrs) do
    reg
    |> cast(attrs, User.reg_fields())
    |> validate_required(User.reg_fields() -- [:middle_name])
    |> validate_format(:email, ~r/@/)
    |> unique_constraint(:email)
  end

  def new_password_changeset(user, attrs) do
    user
    |> cast(attrs, [:password])
    |> validate_required([:password])
  end


  defp put_email_verification_key(changeset) do
    put_change(changeset, :email_verification_key, Auth.gen_key())
  end

  defp put_password_hash(changeset) do
    pwd = get_change(changeset, :password)
    put_change(changeset, :password_hash, hash_password(pwd))
  end

  defp put_temp_password(changeset) do
    put_change(changeset, :password_hash, Auth.gen_key())
  end

  def verify_user(%User{} = user) do
    change(user, %{email_verification_key: nil, verified: true})
    |> Repo.update
  end

  def verify_email_key(%User{email_verification_key: key}, key), do: true
  def verify_email_key(_, _), do: false

  defp hash_password(pwd) when is_binary(pwd) do
    Comeonin.Bcrypt.hashpwsalt(pwd)
  end

  defp hash_password(_), do: nil

  alias His.Accounts.User

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> user_changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> user_changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a User.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    user
    |> change
    |> no_assoc_constraint(:visitations)
    |> Repo.delete()
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{source: %User{}}

  """
  def change_user(%User{} = user) do
    user_changeset(user, %{})
  end

  defp user_changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [:email, :last_name, :first_name, :middle_name, :position])
    |> validate_required([:email, :last_name, :first_name, :position])
  end

  def admins() do
    from(u in User, where: u.position == "admin")
    |> Repo.all()
  end
end

defmodule His.Accounts.Registration do
  use Ecto.Schema

  @fields [:email, :password, :position, :last_name, :first_name, :middle_name]
  embedded_schema do
    field :email,       :string
    field :password,    :string
    field :position,    :string
    field :last_name,   :string
    field :first_name,  :string
    field :middle_name, :string
  end

  def fields, do: @fields
end

defmodule His.Accounts.User do
  use Ecto.Schema

  @positions ~w(admin doctor nurse registrar)
  @reg_fields [:email, :password, :position,
               :last_name, :first_name, :middle_name]

  schema "accounts_users" do
    field :email,         :string
    field :last_name,     :string
    field :first_name,    :string
    field :middle_name,   :string
    field :position,      :string
    field :password,      :string, virtual: true
    field :password_hash, :string

    field :email_verification_key, :string
    field :password_reset_key, :string
    field :verified, :boolean, default: false
    has_many :visitations, His.Patients.Visitation
    timestamps()
  end

  def positions, do: @positions
  def reg_fields, do: @reg_fields
end

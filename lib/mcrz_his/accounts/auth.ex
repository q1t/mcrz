defmodule His.Accounts.Auth do
  def gen_key do
    :crypto.strong_rand_bytes(16)
    |> Base.encode16(case: :lower)
  end
end

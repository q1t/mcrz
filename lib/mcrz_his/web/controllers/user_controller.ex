defmodule His.Web.UserController do
  use His.Web, :controller

  alias His.Accounts

  def index(conn, _params) do
    users = Accounts.list_users()
    render(conn, "index.html", users: users)
  end

  def new(conn, _params) do
    changeset = Accounts.change_user(%His.Accounts.User{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    case Accounts.partial_register_user(user_params) do
      {:ok, user} ->
        His.Email.verification_registration(user)
        |> His.Mailer.deliver_later
        conn
        |> put_flash(:info, "Пользователь зарегистрирован и ему было выслано письмо.")
        |> redirect(to: user_path(conn, :show, user))
      {:error, %Ecto.Changeset{} = changeset} ->
        IO.inspect changeset
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    render(conn, "show.html", user: user)
  end

  def edit(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    changeset = Accounts.change_user(user)
    render(conn, "edit.html", user: user, changeset: changeset)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Accounts.get_user!(id)

    case Accounts.update_user(user, user_params) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "Пользователь успешно обновлен.")
        |> redirect(to: user_path(conn, :show, user))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", user: user, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    case Accounts.delete_user(user) do
      {:ok, _} ->
        conn
        |> put_flash(:info, "Пользователь успешно удален.")
        |> redirect(to: user_path(conn, :index))
      {:error, changeset = %Ecto.Changeset{action: :delete}} ->
        conn
        |> put_flash(:error, "Не удалось удалить пользователя. " <>
                             "Возможно пользователь имеет пациентов.")
        |> redirect(to: patient_path(conn, :index))
    end
  end
end

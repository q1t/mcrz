defmodule His.Web.RegistrationController do
  use His.Web, :controller
  plug :admin_exists
  alias His.Accounts

  def new(conn, _params) do
    changeset = Accounts.change_registration(%His.Accounts.User{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"registration" => registration_params}) do
    case Accounts.register_user(registration_params) do
      {:ok, user} ->
        His.Email.verification(user.email, user.email_verification_key)
        |> His.Mailer.deliver_later
        conn
        |> put_flash(:info, "Вы были успешно зарегистрированы, проверьте вашу почту.")
        |> redirect(to: page_path(conn, :index))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  defp admin_exists(conn, _params) do
    case Accounts.admins() do
      [] -> conn
      _ ->
        conn
        |> put_flash(:error, "Обратитесь к администратору для создания аккаунта.")
        |> redirect(to: page_path(conn, :index))
        |> halt()
    end
  end
end

defmodule His.Web.VisitationController do
  use His.Web, :controller

  alias His.Patients

  def index(conn, _params, patient, current_user) do
    patient = patient |> His.Repo.preload(visitations: His.Patients.visitation_query(:user))
    render(conn, "index.html", patient: patient)
  end

  def new(conn, params, patient, current_user) do
    create(conn, params, patient, current_user)
  end

  def create(conn, _params, patient, current_user) do
    case Patients.create_visitation(patient, current_user, %{"fields" => %{}}) do
      {:ok, visitation} ->
        conn
        |> put_flash(:info, "Прием успешно создан.")
        |> redirect(to: patient_visitation_path(conn, :edit, patient, visitation, part: 1))
      {:error, changeset} ->
        conn
        |> put_flash(:error, "Ошибка при создании приема.")
        |> redirect(to: patient_visitation_path(conn, :index, patient))
    end
  end

  def instructions(conn, %{"id" => id}, patient, _current_user) do
    visitation = Patients.get_visitation!(id)
    changeset = Patients.change_visitation(visitation)
    render(conn, "instructions.html", patient: patient, changeset: changeset)
  end

  def instructions_update(conn, params = %{"id" => id}, patient, _) do
    visitation = Patients.get_visitation!(id)
    case Patients.update_visitation(visitation, params) do
      {:ok, visitation} ->
        conn
        |> put_flash(:info, "Success")
        |> json(%{success: true, follow: patient_visitation_path(conn, :index, patient)})
      {:error, changeset} ->
        conn
        |> put_flash(:error, "Error")
        |> json(%{success: false, follow: patient_visitation_path(conn, :instructions, patient, visitation)})
    end
  end

  def show(conn, %{"id" => id}, patient, current_user) do
    visitation = Patients.get_visitation!(id)
    render(conn, "show.html", patient: patient, visitation: visitation)
  end

  def edit(conn, %{"id" => id, "part" => part}, patient, current_user)
  when part in ~w(1 2 3 4 5) do
    visitation = Patients.get_visitation!(id)
    changeset = Patients.change_visitation(visitation)
    render conn, "edit.html",
           patient: patient,
           visitation: visitation,
           changeset: changeset,
           part: part,
           current_user: current_user
  end

  def edit(conn, %{"id" => id}, patient, _current_user) do
    redirect conn, to: patient_visitation_path(conn, :edit, patient, id, part: "1")
  end

  def update(conn, %{"id" => id, "visitation" => visitation_params, "part" => part}, patient, current_user) do
    visitation = Patients.get_visitation!(id)
    p = String.to_integer(part)
    {redirect_to, flash_text} =
      cond do
        p in 1..4 && visitation.first ->
          {patient_visitation_path(conn, :edit, patient, visitation, part: p + 1),
          "Страниа #{p} према успешно обновлена."}
        true ->
          {patient_visitation_path(conn, :index, patient),
          "Прием успешно сохранен."}
      end
    case Patients.update_visitation(visitation, visitation_params) do
      {:ok, visitation} ->
        conn
        |> put_flash(:info, flash_text)
        |> redirect(to: redirect_to)
      {:error, %Ecto.Changeset{} = changeset} ->
        render conn, "edit.html",
               patient: patient,
               visitation: visitation,
               changeset: changeset,
               part: part,
               current_user: current_user
    end
  end

  def delete(conn, %{"id" => id}, patient, current_user) do
    visitation = Patients.get_visitation!(id)
    {:ok, _visitation} = Patients.delete_visitation(visitation)

    conn
    |> put_flash(:info, "Прием успешно удален.")
    |> redirect(to: patient_visitation_path(conn, :index, patient))
  end

  def print(conn, %{"id" => id}, patient, current_user) do
    visitation = Patients.get_visitation!(id)
    conn
    |> put_layout({His.Web.LayoutView, "document.html"})
    |> render("print.html",
              patient: patient,
              visitation: visitation,
              current_user: current_user)
  end

  def action(conn, _params) do
    id = conn.params["patient_id"]
    patient = Patients.get_patient!(id)
    current_user = conn.assigns[:current_user]
    apply(__MODULE__, action_name(conn), [conn, conn.params, patient, current_user])
  end
end

defmodule His.Web.DocumentController do
  use His.Web, :controller
  alias His.Patients

  def index(conn, %{"patient_id" => id}) do
    patient = Patients.get_patient!(id)
    render conn, "index.html", patient: patient
  end

  def print(conn, %{"id" => id, "patient_id" => patient_id}) do
    patient = Patients.get_patient!(patient_id)
    doc = Patients.get_document!(id)
    conn
    |> put_layout({His.Web.LayoutView, "document.html"})
    |> render(doc.template, patient: patient)
  end

  def new(conn, %{"patient_id" => patient_id}) do
    patient = Patients.get_patient!(patient_id)
    doc = Ecto.build_assoc(patient, :documents)
    changeset = Patients.Document.changeset(doc)
    render conn, "new.html", patient: patient, changeset: changeset
  end

  def create(conn, %{"patient_id" => patient_id, "document" => document_params}) do
    patient = Patients.get_patient!(patient_id)
    case Patients.create_document(patient, document_params) do
      {:ok, doc} ->
        conn
        |> put_flash(:info, "Document created successfully.")
        |> redirect(to: patient_document_path(conn, :show, patient, doc))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", patient: patient, changeset: changeset)
    end
  end

  def show(conn, %{"id" => id, "patient_id" => patient_id}) do
    patient = Patients.get_patient!(patient_id)
    doc = Patients.get_document!(id)
    render(conn, "show.html", document: doc, patient: patient)
  end

  def edit(conn, %{"id" => id, "patient_id" => patient_id}) do
    patient = Patients.get_patient!(patient_id)
    doc = Patients.get_document!(id)
    changeset = His.Patients.Document.changeset(doc)
    render(conn, "edit.html", document: doc, patient: patient, changeset: changeset)
  end

  def update(conn, %{"id" => id, "patient_id" => patient_id, "document" => document_params}) do
    patient = Patients.get_patient!(patient_id)
    doc = Patients.get_document!(id)

    case Patients.update_document(doc, document_params) do
      {:ok, patient} ->
        conn
        |> put_flash(:info, "Document updated successfully.")
        |> redirect(to: patient_document_path(conn, :show, patient_id, doc))
      {:error, %Ecto.Changeset{} = changeset} ->
        IO.inspect changeset
        render(conn, "edit.html", document: doc, patient: patient, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id, "patient_id" => patient_id}) do
    doc = Patients.get_document!(id)
    File.rm!(doc.local_path)
    {:ok, _doc} = His.Repo.delete(doc)

    conn
    |> put_flash(:info, "Document deleted successfully.")
    |> redirect(to: patient_document_path(conn, :index, patient_id))
  end
end

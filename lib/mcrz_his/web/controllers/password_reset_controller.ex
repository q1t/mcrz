defmodule His.Web.PasswordResetController do
  use His.Web, :controller

  def show(conn, _) do
    render conn, "show.html"
  end

  def create(conn, %{"email" => email}) do
    His.Accounts.init_password_reset(email)
    render conn, "create.html"
  end
end

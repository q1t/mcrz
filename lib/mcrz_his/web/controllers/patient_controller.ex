defmodule His.Web.PatientController do
  use His.Web, :controller

  alias His.Patients

  def index(conn, _params) do
    patients = Patients.list_patients()
    render(conn, "index.html", patients: patients)
  end

  def new(conn, _params) do
    changeset = Patients.change_patient(%His.Patients.Patient{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"patient" => patient_params}) do
    case Patients.create_patient(patient_params) do
      {:ok, patient} ->
        conn
        |> put_flash(:info, "Пациент создан успешно.")
        |> redirect(to: patient_path(conn, :show, patient))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    patient = Patients.get_patient!(id)
    render(conn, "show.html", patient: patient)
  end

  def edit(conn, %{"id" => id}) do
    patient = Patients.get_patient!(id)
    changeset = Patients.change_patient(patient)
    render(conn, "edit.html", patient: patient, changeset: changeset)
  end

  def update(conn, %{"id" => id, "patient" => patient_params}) do
    patient = Patients.get_patient!(id)

    case Patients.update_patient(patient, patient_params) do
      {:ok, patient} ->
        conn
        |> put_flash(:info, "Пациент обновлен успешно.")
        |> redirect(to: patient_path(conn, :show, patient))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", patient: patient, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    patient = Patients.get_patient!(id)
    case Patients.delete_patient(patient) do
      {:ok, _patient} -> 
        conn
        |> put_flash(:info, "Пациент удален успешно.")
        |> redirect(to: patient_path(conn, :index))
      {:error, changeset = %Ecto.Changeset{action: :delete}} ->
        conn
        |> put_flash(:error, "Не удалось удалить пациента. " <>
                             "Возможно пациент имеет посещения или документы.")
        |> redirect(to: patient_path(conn, :index))
    end
  end
end

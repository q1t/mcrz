defmodule His.Web.MkbSubsController do
  use His.Web, :controller

  alias His.Patients.MkbSubs

  def index(conn, _params, mkb, _current_user) do
    mkb_subs = MkbSubs.list_mkb_subs(mkb)
    render(conn, "index.html", mkb: mkb, mkb_subs: mkb_subs)
  end

  def new(conn, _params, mkb, _current_user) do
    changeset = MkbSubs.change_mkb_subs(%MkbSubs{})
    render(conn, "new.html", mkb: mkb, changeset: changeset)
  end

  def create(conn, %{"mkb_subs" => mkb_subs_params}, mkb, _current_user) do
    case MkbSubs.create_mkb_subs(mkb, mkb_subs_params) do
      {:ok, mkb_subs} ->
        conn
        |> put_flash(:info, "Mkb subs created successfully.")
        |> redirect(to: mkb_subs_path(conn, :show, mkb, mkb_subs))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", mkb: mkb, changeset: changeset)
    end
  end

  def show(conn, %{"code" => code}, mkb, _current_user) do
    mkb_subs = MkbSubs.get_mkb_subs!(code)
    render(conn, "show.html", mkb: mkb, mkb_subs: mkb_subs)
  end

  def edit(conn, %{"code" => code}, mkb, _current_user) do
    mkb_subs = MkbSubs.get_mkb_subs!(code)
    changeset = MkbSubs.change_mkb_subs(mkb_subs)
    render(conn, "edit.html", mkb: mkb, mkb_subs: mkb_subs, changeset: changeset)
  end

  def update(conn, %{"code" => code, "mkb_subs" => mkb_subs_params}, mkb, _current_user) do
    mkb_subs = MkbSubs.get_mkb_subs!(code)

    case MkbSubs.update_mkb_subs(mkb_subs, mkb_subs_params) do
      {:ok, mkb_subs} ->
        conn
        |> put_flash(:info, "Mkb subs updated successfully.")
        |> redirect(to: mkb_subs_path(conn, :show, mkb, mkb_subs))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", mkb: mkb, mkb_subs: mkb_subs, changeset: changeset)
    end
  end

  def delete(conn, %{"code" => code}, mkb, _current_user) do
    mkb_subs = MkbSubs.get_mkb_subs!(code)
    {:ok, _mkb_subs} = MkbSubs.delete_mkb_subs(mkb_subs)

    conn
    |> put_flash(:info, "Mkb subs deleted successfully.")
    |> redirect(to: mkb_subs_path(conn, :index, mkb))
  end

  def action(conn, _params) do
    code = conn.params["mkb_code"]
    mkb = His.Patients.Mkb.get_mkb!(code)
    current_user = conn.assigns[:current_user]
    apply(__MODULE__, action_name(conn), [conn, conn.params, mkb, current_user])
  end
end

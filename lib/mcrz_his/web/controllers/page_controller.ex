defmodule His.Web.PageController do
  use His.Web, :controller

  plug :already_logged

  def index(conn, _params) do
    render conn, "index.html"
  end

  def already_logged(conn, _opts) do
    if conn.assigns[:current_user] do
      conn
      |> redirect(to: dashboard_path(conn, :show))
    else
      conn
    end
  end
end

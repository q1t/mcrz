defmodule His.Web.ComplaintsController do
  use His.Web, :controller

  alias His.App

  def index(conn, _params, category) do
    complaints = category.complaints
    render(conn, "index.html", complaints: complaints, category: category)
  end

  def new(conn, _params, category) do
    changeset = App.change_complaints(%His.App.Complaints{})
    render(conn, "new.html", changeset: changeset, category: category)
  end

  def create(conn, %{"complaints" => complaints_params}, category) do
    case App.create_complaints(category, complaints_params) do
      {:ok, complaints} ->
        conn
        |> put_flash(:info, "Complaints created successfully.")
        |> redirect(to: complaint_categories_complaints_path(conn, :show, category, complaints))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset, category: category)
    end
  end

  def show(conn, %{"id" => id}, category) do
    complaints = App.get_complaints!(id)
    render(conn, "show.html", complaints: complaints, category: category)
  end

  def edit(conn, %{"id" => id}, category) do
    complaints = App.get_complaints!(id)
    changeset = App.change_complaints(complaints)
    render(conn, "edit.html", complaints: complaints, changeset: changeset, category: category)
  end

  def update(conn, %{"id" => id, "complaints" => complaints_params}, category) do
    complaints = App.get_complaints!(id)

    case App.update_complaints(complaints, complaints_params) do
      {:ok, complaints} ->
        conn
        |> put_flash(:info, "Complaints updated successfully.")
        |> redirect(to: complaint_categories_complaints_path(conn, :show, category, complaints))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html",
          complaints: complaints, changeset: changeset, category: category)
    end
  end

  def delete(conn, %{"id" => id}, category) do
    complaints = App.get_complaints!(id)
    {:ok, _complaints} = App.delete_complaints(complaints)

    conn
    |> put_flash(:info, "Complaints deleted successfully.")
    |> redirect(to: complaint_categories_complaints_path(conn, :index, category))
  end

  def action(conn, _params) do
    id = conn.params["complaint_categories_id"]
    category = His.App.get_complaint_categories!(id)
    current_user = conn.assigns[:current_user]
    apply(__MODULE__, action_name(conn), [conn, conn.params, category])
  end
end

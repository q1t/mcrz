defmodule His.Web.SessionController do
  use His.Web, :controller
  alias His.Sessions

  def create(conn, %{"email" => email, "password" => password}) do
    case Sessions.login(email, password) do
      {:ok, user} ->
        conn
        |> put_session(:user_id, user.id)
        |> redirect(to: dashboard_path(conn, :show))
      {:error, _reason} ->
        conn
        |> put_flash(:error, "Неправильный логин или пароль.")
        |> redirect(to: page_path(conn, :index))
    end
  end

  def delete(conn, _params) do
    conn
    |> delete_session(:user_id)
    |> redirect(to: page_path(conn, :index))
  end
end

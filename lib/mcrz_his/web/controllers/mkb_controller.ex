defmodule His.Web.MkbController do
  use His.Web, :controller

  alias His.Patients.Mkb

  def index(conn, _params) do
    mkbs = Mkb.list_mkbs()
    render(conn, "index.html", mkbs: mkbs)
  end

  def new(conn, _params) do
    changeset = Mkb.change_mkb(%Mkb{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"mkb" => mkb_params}) do
    case Mkb.create_mkb(mkb_params) do
      {:ok, mkb} ->
        conn
        |> put_flash(:info, "Mkb created successfully.")
        |> redirect(to: mkb_path(conn, :show, mkb))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"code" => code}) do
    mkb = Mkb.get_mkb!(code)
    render(conn, "show.html", mkb: mkb)
  end

  def edit(conn, %{"code" => code}) do
    mkb = Mkb.get_mkb!(code)
    changeset = Mkb.change_mkb(mkb)
    render(conn, "edit.html", mkb: mkb, changeset: changeset)
  end

  def update(conn, %{"code" => code, "mkb" => mkb_params}) do
    mkb = Mkb.get_mkb!(code)

    case Mkb.update_mkb(mkb, mkb_params) do
      {:ok, mkb} ->
        conn
        |> put_flash(:info, "Mkb updated successfully.")
        |> redirect(to: mkb_path(conn, :show, mkb))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", mkb: mkb, changeset: changeset)
    end
  end

  def delete(conn, %{"code" => code}) do
    mkb = Mkb.get_mkb!(code)
    {:ok, _mkb} = Mkb.delete_mkb(mkb)

    conn
    |> put_flash(:info, "Mkb deleted successfully.")
    |> redirect(to: mkb_path(conn, :index))
  end
end

defmodule His.Web.PasswordController do
  use His.Web, :controller
  alias His.Accounts

  def show(conn, %{"email" => email, "key" => key}) do
    conn
    |> put_session(:reset_email, email)
    |> put_session(:reset_key, key)
    |> redirect(to: password_path(conn, :show))
  end

  def show(conn, _) do
    email = get_session(conn, :reset_email)
    key = get_session(conn, :reset_key)
    changeset = Accounts.new_password()
    render conn, "show.html", changeset: changeset, key: key, email: email
  end

  def update(conn, %{"user" => user_params}) do
    email = user_params["email"]
    key = user_params["key"]

    case Accounts.update_password(email, key, user_params) do
      :ok ->
        conn
        |> clear_session()
        |> configure_session(renew: true)
        |> put_flash(:info, "Ваш новый пароль был сохранен.")
        |> redirect(to: page_path(conn, :index))
      :error ->
        conn
        |> put_flash(:error, "Произошла ошибка.")
        |> redirect(to: page_path(conn, :index))
      {:error, changeset} ->
        render conn, "show.html", changeset: changeset, key: key, email: email
    end
  end

  def update(conn, params) do
    IO.inspect params
  end
end

defmodule His.Web.ComplaintCategoriesController do
  use His.Web, :controller

  alias His.App

  def index(conn, _params) do
    complaint_categories = App.list_complaint_categories()
    render(conn, "index.html", complaint_categories: complaint_categories)
  end

  def new(conn, _params) do
    changeset = App.change_complaint_categories(%His.App.ComplaintCategories{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"complaint_categories" => complaint_categories_params}) do
    case App.create_complaint_categories(complaint_categories_params) do
      {:ok, complaint_categories} ->
        conn
        |> put_flash(:info, "Complaint categories created successfully.")
        |> redirect(to: complaint_categories_path(conn, :show, complaint_categories))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    complaint_categories = App.get_complaint_categories!(id)
    render(conn, "show.html", complaint_categories: complaint_categories)
  end

  def edit(conn, %{"id" => id}) do
    complaint_categories = App.get_complaint_categories!(id)
    changeset = App.change_complaint_categories(complaint_categories)
    render(conn, "edit.html", complaint_categories: complaint_categories, changeset: changeset)
  end

  def update(conn, %{"id" => id, "complaint_categories" => complaint_categories_params}) do
    complaint_categories = App.get_complaint_categories!(id)

    case App.update_complaint_categories(complaint_categories, complaint_categories_params) do
      {:ok, complaint_categories} ->
        conn
        |> put_flash(:info, "Complaint categories updated successfully.")
        |> redirect(to: complaint_categories_path(conn, :show, complaint_categories))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", complaint_categories: complaint_categories, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    complaint_categories = App.get_complaint_categories!(id)
    {:ok, _complaint_categories} = App.delete_complaint_categories(complaint_categories)

    conn
    |> put_flash(:info, "Complaint categories deleted successfully.")
    |> redirect(to: complaint_categories_path(conn, :index))
  end
end

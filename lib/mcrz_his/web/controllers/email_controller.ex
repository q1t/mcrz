defmodule His.Web.EmailController do
  use His.Web, :controller
  alias His.Accounts.User
  alias His.Accounts

  def verify(conn, %{"email" => email, "key" => key, "pw_key" => pw_key}) do
    IO.inspect pw_key
    with user = %User{} <- Accounts.get_user_by_email(email),
         true <- Accounts.verify_email_key(user, key),
         {:ok, user} <- Accounts.verify_user(user) do
      conn
      |> put_flash(:info, "Ваша почта подтверждена, установите новый пароль.")
      |> redirect(to: password_path(conn, :show, email: email, key: pw_key))
    else
      _ ->
        conn
        |> put_flash(:error, "Ошибка подтверждения почты.")
        |> redirect(to: page_path(conn, :index))
    end
  end

  def verify(conn, %{"email" => email, "key" => key}) do
    with user = %User{} <- Accounts.get_user_by_email(email),
         true <- Accounts.verify_email_key(user, key),
         {:ok, user} <- Accounts.verify_user(user) do
      conn
      |> put_flash(:info, "Почта подтверждена.")
      |> put_session(:user_id, user.id)
      |> redirect(dashboard_path(conn, :show))
    else
      _ ->
        conn
        |> put_flash(:error, "Ошибка подтверждения почты.")
        |> redirect(to: page_path(conn, :index))
    end
  end
end

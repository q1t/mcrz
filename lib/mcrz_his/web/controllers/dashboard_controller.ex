defmodule His.Web.DashboardController do
  use His.Web, :controller

  def show(conn, _params, current_user) do
    current_user = current_user |> His.Repo.preload(visitations: His.Patients.visitation_query(:patient))
    render conn, "show.html", user: current_user
  end

  def action(conn, _params) do
    current_user = conn.assigns[:current_user]
    apply(__MODULE__, action_name(conn), [conn, conn.params, current_user])
  end
end

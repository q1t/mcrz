defmodule His.Web.Router do
  use His.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug His.Sessions.Login
  end

  pipeline :auth do
    plug His.Sessions.Logged
  end

  pipeline :admin do
    plug His.Sessions.AdminOnly
  end

  scope "/admin", His.Web do
    pipe_through [:browser, :auth, :admin]

    resources "/mkb", MkbController, param: "code" do
      resources "/sub_code", MkbSubsController, param: "code", as: "subs"
    end

    scope "/complaints" do
      resources "/categories", ComplaintCategoriesController do
        resources "/c", ComplaintsController
      end
    end

    resources "/users", UserController
  end

  scope "/dashboard", His.Web do
    pipe_through [:browser, :auth]

    get "/", DashboardController, :show

    scope "/resources" do
      resources "/patients", PatientController do
        resources "/visitations", VisitationController
        get "/visitations/:id/instructions", VisitationController, :instructions
        put "/visitations/:id/instructions", VisitationController, :instructions_update
        get "/visitations/:id/print",        VisitationController, :print
        resources "/documents", DocumentController
        get "/documents/:id/print", DocumentController, :print
      end
    end
  end

  scope "/", His.Web do
    pipe_through :browser

    get "/", PageController, :index

    resources "/registration", RegistrationController,
      only: [:new, :create], singleton: true

    post "/session/create", SessionController, :create
    get  "/session/delete", SessionController, :delete

    get  "/email/verify", EmailController, :verify

    get  "/password/new", PasswordController, :show
    post "/password/new", PasswordController, :update
    put  "/password/new", PasswordController, :update

    get  "/password/reset", PasswordResetController, :show
    post "/passwrod/reset", PasswordResetController, :create
  end

  scope "/" do
    if Mix.env == :dev do
      forward "/sent_emails", Bamboo.EmailPreviewPlug
    end
  end
end

defmodule His.Web.DashboardView do
  use His.Web, :view

  def tr_position("doctor"), do: "доктор"
  def tr_position("nurse"), do: "мед. сестра"
  def tr_position("admin"), do: "администратор"
  def tr_position("registrar"), do: "регистратор"
end

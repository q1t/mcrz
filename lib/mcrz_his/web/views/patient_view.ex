defmodule His.Web.PatientView do
  use His.Web, :view

  def current_year, do: Date.utc_today |> Map.get(:year)
end

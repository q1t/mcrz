defmodule His.Web.VisitationView do
  use His.Web, :view
  import His.Web.VisitationPrintView,
    only: [prepare_for_print: 1, section: 1, render_if_exists: 3, render_if_exists: 2]

  def boxes(form, map, field, options, opts \\ []) do
    Enum.map(options, fn option ->
      content_tag :label,
      [map_box(form, map, field, option, opts),
       content_tag(:span, option)]
    end)
  end

  defp map_box(form, map, field, value, opts) do
    data = Map.get(form.data, map)[field][value] || ""
    type = opts[:using] || :checkbox
    name = "visitation[#{map}][#{field}][#{value}]"
    checked_value = value
    apply(Phoenix.HTML.Form, type,
      [form, type,
       [value: data, name: name, class: opts[:class], checked_value: checked_value]])
  end

  def sub_radios(form, map, field, sub, options, opts \\ []) do
    radios = make_radios(form, map, field <> ":" <> sub, options, opts)
    if opts[:skip_title] do
      radios
    else
      [content_tag(:span, sub, class: "sub"), radios]
    end
    |> wrap_group
  end

  def radios(form, map, field, options, opts \\ []) do
    radios = make_radios(form, map, field, options, opts)
    if opts[:skip_title] do
      radios
    else
      [content_tag(:span, field), radios]
    end
    |> wrap_group
  end

  defp wrap_group(data) do
    content_tag(:div, data, class: "radios-group")
  end

  def make_radios(form, map, field, options, opts) do
      Enum.map(options, fn option ->
        content_tag :label,
        [map_radio(form, map, field, option, opts),
        content_tag(:span, option)]
      end)
  end

  def list(form, map, label, collection, opts \\ []) do
    content_tag :div, [class: "list"] do
      div = content_tag :div, [class: "multiple-input"] do
        select = multiple_select :noop, :noop, collection, opts[:select] || []
        input = map_input form, map, label, [using: :textarea]
        [select, input]
      end
      [~E"<label> <%= label %> </label>", div]
    end
  end

  def prepare_complaints(complaints) do
    complaints
    |> Enum.map(fn category ->
      {category.title, category.complaints |> Enum.map(&{&1.body, &1.body})}
    end)
  end

  def prepare_mkbs(mkbs) do
    mkbs
    |> Enum.map(fn mkb ->
      subs =
        if length(mkb.sub_codes) > 0 do
          mkb.sub_codes |> Enum.map(&{prepare_mkb(&1), prepare_mkb(&1)})
        else
          [prepare_mkb(mkb)]
        end
      {prepare_mkb(mkb), subs}
    end)
  end

  defp prepare_mkb(mkb) do
    "#{mkb.code} | #{mkb.short_desc}"
  end

  def map(form, map, field, opts \\ []) do
    content_tag :div, [class: "form-group"] do
      label = label(form, map, field, class: "control-label")
      input = map_input(form, map, field, opts)
      [label, input]
    end
  end

  def map_radio(form, map, field, value, opts \\ []) do
    data = Map.get(form.data, map)[field] || ""
    type = opts[:using] || :radio_button
    name = "visitation[#{map}][#{field}]"
    checked? = value == data
    apply(Phoenix.HTML.Form, type,
      [form, type, value,
       [value: data, name: name, class: opts[:class], checked: checked?]])
  end

  def map_input(form, map, field, opts \\ []) do
    data = Map.get(form.data, map)[field] || ""
    type = opts[:using] || :textarea
    name =
      unless type == :multiple_select do
        "visitation[#{map}][#{field}]"
      else
        "visitation[#{map}][#{field}][]"
      end
    {select_from, opts} = Keyword.pop(opts, :select_from)
    if select_from do
      apply(Phoenix.HTML.Form, type,
        [form, type, select_from,
         [name: name, value: data, class: opts[:class] || "form-control"]])
    else
      apply(Phoenix.HTML.Form, type,
        [form, type,
         [name: name,
          value: data,
          class: opts[:class] || "form-control",
          placeholder: opts[:placeholder] || ""]])
    end
  end

  def partial_template(part) do
    "part_#{part}.html"
  end

  def render_pagination(conn, action, patient, nil, part) do
    fun = &(patient_visitation_path(conn, action, patient, part: &1))
    render_pagination(fun, part)
  end

  def render_pagination(conn, action, patient, visitation, part) do
    fun = &(patient_visitation_path(conn, action, patient, visitation, part: &1))
    render_pagination(fun, part)
  end

  defp render_pagination(fun, part) do
    parts = ~w(1 2 3 4 5)
    content_tag :nav do
      content_tag :ul, [class: "pagination"] do
        Enum.map(parts, fn p ->
          state = if p == part, do: "active", else: ""
          to = "Страница #{p}"
          content_tag :li, [class: state] do
            link to, to: fun.(p)
          end
        end)
      end
    end
  end

  def submit_text(false, _), do: "Сохранить"
  def submit_text(_, "5"), do: "Сохранить"
  def submit_text(true, _), do: "Сохранить и перейти к след."
end

defmodule His.Web.ViewHelpers do
  def logged_in?(conn) do
    !!conn.assigns[:current_user]
  end

  def is_admin?(%His.Accounts.User{position: "admin"}), do: true
  def is_admin?(_), do: false

  def format_time(time) do
    Timex.format!(DateTime.utc_now, "%d.%m.%Y %H.%M", :strftime)
  end

  def lfm(user) do
    "#{user.last_name} #{user.first_name} #{user.middle_name}"
  end
end

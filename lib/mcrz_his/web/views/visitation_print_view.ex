defmodule His.Web.VisitationPrintView do
  use His.Web, :view

  def render_if_exists(group_id, data, fun) when is_atom(group_id) do
    if group = data[group_id] do
      content_tag :p do
        [content_tag(:h2, map_group(group_id)),
        fun.(group)]
      end
    end
  end

  def render_if_exists(field, data, pattern \\ nil) do
    if child = Enum.find(data, fn {f, _} -> f == field end) do
      render_child(child, pattern)
    end
  end

  def section({sec, children}) do
    header = content_tag :h2, do: sec
    content_tag :p do
      [header,
      Enum.map(children, &render_child(&1, nil))]
    end
  end

  def render_child({field, value}, tmpl) when is_map(value) do
    value = for {k, v} <- value, v != "false", do: k
    render_child({field, Enum.join(value, ", ")}, tmpl)
  end

  def render_child({field, value}, tmpl) when is_binary(value) do
    if tmpl do
      content_tag :span, class: ["child"] do
        String.replace(tmpl, "{}", value)
      end
    else
      f = content_tag :strong, field <> ":", class: "child_field"
      v = content_tag :span, value, class: "child_value"
      content_tag :span, [class: "child"] do
        [f, v]
      end
    end
  end

  def prepare_for_print(visitation) do
    visitation
    |> IO.inspect
    |> Map.take([:so, :ab, :ds, :sg, :ou, :zk, :jb, :po])
    |> Enum.sort_by(fn {k, _} -> order(k) end)
    |> Enum.reject(&match?({_, nil}, &1))
    |> Enum.map(fn {k, v} -> {k, Enum.reject(v, &match?({_, ""}, &1))} end)
    |> Enum.reject(&match?({_, []}, &1))
  end

  defp map_group(:so), do: "Сигнальные отметки"
  defp map_group(:ab), do: "Общее"
  defp map_group(:ds), do: "Данные объективного осмотра"
  defp map_group(:sg), do: "Status Genetals"
  defp map_group(:ou), do: "Осмотр уролога"
  defp map_group(:zk), do: "Заключение"
  defp map_group(:jb), do: "Жалобы, рекомендации и назначения"
  defp map_group(:po), do: "План обследования"

  defp order(:so), do: 1
  defp order(:ab), do: 2
  defp order(:ds), do: 3
  defp order(:sg), do: 4
  defp order(:ou), do: 5
  defp order(:zk), do: 6
  defp order(:jb), do: 7
  defp order(:po), do: 8
end

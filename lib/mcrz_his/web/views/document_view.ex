defmodule His.Web.DocumentView do
  use His.Web, :view

  def full_name(%His.Patients.Patient{} = p) do
    "#{p.last_name} #{p.first_name} #{p.middle_name}"
  end

  def today do
    Date.utc_today |> format_date()
  end

  def format_date(%{day: day, month: month, year: year}) do
    month = humanize_month(month)
    "«#{day}» #{month} #{year} г."
  end

  def humanize_month(1), do: "Января"
  def humanize_month(2), do: "Февраля"
  def humanize_month(3), do: "Марта"
  def humanize_month(4), do: "Апреля"
  def humanize_month(5), do: "Мая"
  def humanize_month(6), do: "Июня"
  def humanize_month(7), do: "Июля"
  def humanize_month(8), do: "Августа"
  def humanize_month(9), do: "Сентября"
  def humanize_month(10), do: "Октября"
  def humanize_month(11), do: "Ноября"
  def humanize_month(12), do: "Декабря"

  def local_path(%His.Patients.Document{local_path: path}) do
    "/uploads/documents/#{Path.basename(path)}"
  end
end

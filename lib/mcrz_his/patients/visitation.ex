defmodule His.Patients.Visitation do
  use Ecto.Schema

  schema "patients_visitations" do
    field :so, :map
    field :ab, :map
    field :ds, :map
    field :sg, :map
    field :ou, :map
    field :zk, :map
    field :jb, :map
    field :po, :map
    field :fields, :map
    field :instructions, :map
    field :next_visit_at, :utc_datetime
    field :first, :boolean, default: false

    belongs_to :patient, His.Patients.Patient
    belongs_to :user, His.Accounts.User

    timestamps()
  end
end

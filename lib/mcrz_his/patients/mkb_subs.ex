defmodule His.Patients.MkbSubs do
  use Ecto.Schema
  import Ecto.{Changeset, Query}, warn: false
  alias His.Repo

  @derive {Phoenix.Param, key: :code}
  schema "patients_mkb_sub_codes" do
    field :code, :string
    field :short_desc, :string
    belongs_to :mkb, His.Patients.Mkb

    timestamps()
  end

  alias His.Patients.MkbSubs

  @doc """
  Returns the list of mkb_subs.

  ## Examples

      iex> list_mkb_subs()
      [%MkbSubs{}, ...]

  """
  def list_mkb_subs do
    Repo.all(MkbSubs)
  end

  def list_mkb_subs(mkb) do
    Ecto.assoc(mkb, :sub_codes) 
    |> Ecto.Query.order_by([s], [s.code]) 
    |> His.Repo.all
  end

  @doc """
  Gets a single mkb_subs.

  Raises `Ecto.NoResultsError` if the Mkb subs does not exist.

  ## Examples

      iex> get_mkb_subs!(123)
      %MkbSubs{}

      iex> get_mkb_subs!(456)
      ** (Ecto.NoResultsError)

  """
  def get_mkb_subs!(code), do: Repo.get_by!(MkbSubs, code: code)

  @doc """
  Creates a mkb_subs.

  ## Examples

      iex> create_mkb_subs(%{field: value})
      {:ok, %MkbSubs{}}

      iex> create_mkb_subs(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_mkb_subs(mkb, attrs \\ %{}) do
    Ecto.build_assoc(mkb, :sub_codes)
    |> mkb_subs_changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a mkb_subs.

  ## Examples

      iex> update_mkb_subs(mkb_subs, %{field: new_value})
      {:ok, %MkbSubs{}}

      iex> update_mkb_subs(mkb_subs, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_mkb_subs(%MkbSubs{} = mkb_subs, attrs) do
    mkb_subs
    |> mkb_subs_changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a MkbSubs.

  ## Examples

      iex> delete_mkb_subs(mkb_subs)
      {:ok, %MkbSubs{}}

      iex> delete_mkb_subs(mkb_subs)
      {:error, %Ecto.Changeset{}}

  """
  def delete_mkb_subs(%MkbSubs{} = mkb_subs) do
    Repo.delete(mkb_subs)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking mkb_subs changes.

  ## Examples

      iex> change_mkb_subs(mkb_subs)
      %Ecto.Changeset{source: %MkbSubs{}}

  """
  def change_mkb_subs(%MkbSubs{} = mkb_subs) do
    mkb_subs_changeset(mkb_subs, %{})
  end

  defp mkb_subs_changeset(%MkbSubs{} = mkb_subs, attrs) do
    mkb_subs
    |> cast(attrs, [:code, :short_desc])
    |> validate_required([:code, :short_desc])
    |> unique_constraint(:code)
    |> assoc_constraint(:mkb)
    |> update_change(:code, &prepare_code/1)
  end

  defp prepare_code(code), do: code |> String.trim() |> String.upcase()
end

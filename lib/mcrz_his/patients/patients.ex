defmodule His.Patients do
  @moduledoc """
  The boundary for the Patients system.
  """

  import Ecto.{Query, Changeset}, warn: false
  alias His.Repo
  alias His.Patients.Document
  alias His.Patients.Patient

  def list_patients do
    Repo.all(Patient)
  end

  def get_patient!(id), do: Repo.get!(Patient, id) |> Repo.preload(:documents)

  def create_patient(attrs \\ %{}) do
    %Patient{}
    |> patient_changeset(attrs, :create)
    |> Repo.insert()
  end

  def update_patient(%Patient{} = patient, attrs) do
    patient
    |> patient_changeset(attrs, :update)
    |> Repo.update()
  end

  def delete_patient(%Patient{} = patient) do
    patient
    |> change()
    |> no_assoc_constraint(:visitations)
    |> Repo.delete()
  end

  def change_patient(%Patient{} = patient) do
    patient_changeset(patient, %{})
  end

  defp patient_changeset(%Patient{} = patient, attrs, :update) do
    patient_changeset(patient, attrs)
    |> cast_assoc(:documents, with: &Document.update_changeset/2)
  end

  defp patient_changeset(%Patient{} = patient, attrs, :create) do
    patient_changeset(patient, attrs)
    |> put_assoc(:documents, default_documents())
  end

  defp patient_changeset(%Patient{} = patient, attrs) do
    patient
    |> cast(attrs, [:email, :last_name, :first_name, :middle_name, :code, :address, :phone_number, :birthday, :first_visit])
    |> validate_required([:first_name])
  end


  defp default_documents do
    [%Document{template: "agreement.html", name: "Договор"}]
  end


  def get_document!(id), do: Repo.get!(Document, id)

  def create_document(patient, attrs \\ %{}) do
    Ecto.build_assoc(patient, :documents)
    |> Document.changeset(attrs)
    |> Repo.insert()
  end

  def update_document(doc, attrs) do
    doc
    |> Document.update_changeset(attrs)
    |> Repo.update
  end

  alias His.Patients.Visitation

  def visitation_query(preload) do
    from(v in Visitation, order_by: v.inserted_at, preload: [^preload])
  end

  def list_visitations do
    Repo.all(Visitation)
  end

  def get_visitation!(id), do: Repo.get!(Visitation, id)

  def create_visitation(patient, user, attrs \\ %{}) do
    Ecto.build_assoc(patient, :visitations, %{user_id: user.id})
    |> visitation_changeset(attrs)
    |> first?(patient)
    |> Repo.insert()
  end

  def first?(changeset, patient) do
    first = !has_first?(patient)
    put_change(changeset, :first, first)
  end

  def has_first?(patient) do
    visitation = 
      Ecto.assoc(patient, :visitations)
      |> where([v], v.first == true)
      |> Repo.one
    !is_nil(visitation)
  end

  def update_visitation(%Visitation{} = visitation, attrs) do
    attrs = Map.put(attrs, "fields", Map.merge(visitation.fields, attrs["fields"] || %{}))
    visitation
    |> visitation_changeset(attrs)
    |> Repo.update()
  end

  def delete_visitation(%Visitation{} = visitation) do
    Repo.delete(visitation)
  end

  def change_visitation(%Visitation{} = visitation) do
    visitation_changeset(visitation, %{})
  end

  defp visitation_changeset(%Visitation{} = visitation, attrs) do
    visitation
    |> cast(attrs, [:fields,
                   :instructions,
                   :next_visit_at,
                   :so,
                   :ab,
                   :ds,
                   :sg,
                   :ou,
                   :zk,
                   :jb,
                   :po])
  end


  alias His.Patients.Mkb

  @doc """
  Returns the list of mkb_subs.

  ## Examples

      iex> list_mkb_subs()
      [%Mkb{}, ...]

  """
  def list_mkb_subs do
    Repo.all(Mkb)
  end

  @doc """
  Gets a single mkb.

  Raises `Ecto.NoResultsError` if the Mkb does not exist.

  ## Examples

      iex> get_mkb!(123)
      %Mkb{}

      iex> get_mkb!(456)
      ** (Ecto.NoResultsError)

  """
  def get_mkb!(id), do: Repo.get!(Mkb, id)

  @doc """
  Creates a mkb.

  ## Examples

      iex> create_mkb(%{field: value})
      {:ok, %Mkb{}}

      iex> create_mkb(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_mkb(attrs \\ %{}) do
    %Mkb{}
    |> mkb_changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a mkb.

  ## Examples

      iex> update_mkb(mkb, %{field: new_value})
      {:ok, %Mkb{}}

      iex> update_mkb(mkb, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_mkb(%Mkb{} = mkb, attrs) do
    mkb
    |> mkb_changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Mkb.

  ## Examples

      iex> delete_mkb(mkb)
      {:ok, %Mkb{}}

      iex> delete_mkb(mkb)
      {:error, %Ecto.Changeset{}}

  """
  def delete_mkb(%Mkb{} = mkb) do
    Repo.delete(mkb)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking mkb changes.

  ## Examples

      iex> change_mkb(mkb)
      %Ecto.Changeset{source: %Mkb{}}

  """
  def change_mkb(%Mkb{} = mkb) do
    mkb_changeset(mkb, %{})
  end

  defp mkb_changeset(%Mkb{} = mkb, attrs) do
    mkb
    |> cast(attrs, [:code, :short_desc])
    |> validate_required([:code, :short_desc])
  end
end

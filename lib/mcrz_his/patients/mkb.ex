defmodule His.Patients.Mkb do
  import Ecto.{Changeset, Query}, warn: false
  alias His.Repo
  use Ecto.Schema

  @derive {Phoenix.Param, key: :code}
  schema "patients_mkb_codes" do
    field :code, :string
    field :short_desc, :string
    has_many :sub_codes, His.Patients.MkbSubs

    timestamps()
  end

  alias His.Patients.Mkb

  @doc """
  Returns the list of patients.

  ## Examples

      iex> list_patients()
      [%Mkb{}, ...]

  """
  def list_mkbs do
    from(m in Mkb,
      order_by: m.code,
      left_join: s in assoc(m, :sub_codes),
      order_by: s.code,
      preload: [sub_codes: s])
   |> Repo.all
  end

  @doc """
  Gets a single mkb.

  Raises `Ecto.NoResultsError` if the Mkb does not exist.

  ## Examples

      iex> get_mkb!(123)
      %Mkb{}

      iex> get_mkb!(456)
      ** (Ecto.NoResultsError)

  """
  def get_mkb!(code), do: Repo.get_by!(Mkb, code: code)

  @doc """
  Creates a mkb.

  ## Examples

      iex> create_mkb(%{field: value})
      {:ok, %Mkb{}}

      iex> create_mkb(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_mkb(attrs \\ %{}) do
    %Mkb{}
    |> mkb_changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a mkb.

  ## Examples

      iex> update_mkb(mkb, %{field: new_value})
      {:ok, %Mkb{}}

      iex> update_mkb(mkb, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_mkb(%Mkb{} = mkb, attrs) do
    mkb
    |> mkb_changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Mkb.

  ## Examples

      iex> delete_mkb(mkb)
      {:ok, %Mkb{}}

      iex> delete_mkb(mkb)
      {:error, %Ecto.Changeset{}}

  """
  def delete_mkb(%Mkb{} = mkb) do
    Repo.delete(mkb)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking mkb changes.

  ## Examples

      iex> change_mkb(mkb)
      %Ecto.Changeset{source: %Mkb{}}

  """
  def change_mkb(%Mkb{} = mkb) do
    mkb_changeset(mkb, %{})
  end

  defp mkb_changeset(%Mkb{} = mkb, attrs) do
    mkb
    |> cast(attrs, [:code, :short_desc])
    |> validate_required([:code, :short_desc])
    |> unique_constraint(:code)
    |> update_change(:code, &prepare_code/1)
  end

  defp prepare_code(code), do: code |> String.trim() |> String.upcase()
end

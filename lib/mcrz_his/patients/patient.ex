defmodule His.Patients.Patient do
  use Ecto.Schema

  schema "patients_patients" do
    field :address, :string
    field :birthday, :date
    field :code, :string
    field :email, :string
    field :first_name, :string
    field :first_visit, :naive_datetime
    field :last_name, :string
    field :middle_name, :string
    field :phone_number, :string

    has_many :documents, His.Patients.Document
    has_many :visitations, His.Patients.Visitation
    timestamps()
  end
end

defmodule His.App do
  @moduledoc """
  The boundary for the App system.
  """

  import Ecto.{Query, Changeset}, warn: false
  alias His.Repo
  alias His.App.Complaints

  @doc """
  Returns the list of complaints.

  ## Examples

      iex> list_complaints()
      [%Complaints{}, ...]

  """
  def list_complaints do
    Repo.all(Complaints)
  end

  @doc """
  Gets a single complaints.

  Raises `Ecto.NoResultsError` if the Complaints does not exist.

  ## Examples

      iex> get_complaints!(123)
      %Complaints{}

      iex> get_complaints!(456)
      ** (Ecto.NoResultsError)

  """
  def get_complaints!(id), do: Repo.get!(Complaints, id)

  @doc """
  Creates a complaints.

  ## Examples

      iex> create_complaints(%{field: value})
      {:ok, %Complaints{}}

      iex> create_complaints(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_complaints(category, attrs \\ %{}) do
    Ecto.build_assoc(category, :complaints)
    |> complaints_changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a complaints.

  ## Examples

      iex> update_complaints(complaints, %{field: new_value})
      {:ok, %Complaints{}}

      iex> update_complaints(complaints, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_complaints(%Complaints{} = complaints, attrs) do
    complaints
    |> complaints_changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Complaints.

  ## Examples

      iex> delete_complaints(complaints)
      {:ok, %Complaints{}}

      iex> delete_complaints(complaints)
      {:error, %Ecto.Changeset{}}

  """
  def delete_complaints(%Complaints{} = complaints) do
    Repo.delete(complaints)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking complaints changes.

  ## Examples

      iex> change_complaints(complaints)
      %Ecto.Changeset{source: %Complaints{}}

  """
  def change_complaints(%Complaints{} = complaints) do
    complaints_changeset(complaints, %{})
  end

  defp complaints_changeset(%Complaints{} = complaints, attrs) do
    complaints
    |> cast(attrs, [:body])
    |> validate_required([:body])
  end

  alias His.App.ComplaintCategories

  @doc """
  Returns the list of complaint_categories.

  ## Examples

      iex> list_complaint_categories()
      [%ComplaintCategories{}, ...]

  """
  def list_complaint_categories do
    Repo.all(ComplaintCategories) |> Repo.preload(:complaints)
  end

  @doc """
  Gets a single complaint_categories.

  Raises `Ecto.NoResultsError` if the Complaint categories does not exist.

  ## Examples

      iex> get_complaint_categories!(123)
      %ComplaintCategories{}

      iex> get_complaint_categories!(456)
      ** (Ecto.NoResultsError)

  """
  def get_complaint_categories!(id), do: Repo.get!(ComplaintCategories, id) |> Repo.preload(:complaints)

  @doc """
  Creates a complaint_categories.

  ## Examples

      iex> create_complaint_categories(%{field: value})
      {:ok, %ComplaintCategories{}}

      iex> create_complaint_categories(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_complaint_categories(attrs \\ %{}) do
    %ComplaintCategories{}
    |> complaint_categories_changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a complaint_categories.

  ## Examples

      iex> update_complaint_categories(complaint_categories, %{field: new_value})
      {:ok, %ComplaintCategories{}}

      iex> update_complaint_categories(complaint_categories, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_complaint_categories(%ComplaintCategories{} = complaint_categories, attrs) do
    complaint_categories
    |> complaint_categories_changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a ComplaintCategories.

  ## Examples

      iex> delete_complaint_categories(complaint_categories)
      {:ok, %ComplaintCategories{}}

      iex> delete_complaint_categories(complaint_categories)
      {:error, %Ecto.Changeset{}}

  """
  def delete_complaint_categories(%ComplaintCategories{} = complaint_categories) do
    Repo.delete(complaint_categories)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking complaint_categories changes.

  ## Examples

      iex> change_complaint_categories(complaint_categories)
      %Ecto.Changeset{source: %ComplaintCategories{}}

  """
  def change_complaint_categories(%ComplaintCategories{} = complaint_categories) do
    complaint_categories_changeset(complaint_categories, %{})
  end

  defp complaint_categories_changeset(%ComplaintCategories{} = complaint_categories, attrs) do
    complaint_categories
    |> cast(attrs, [:title])
    |> validate_required([:title])
  end
end

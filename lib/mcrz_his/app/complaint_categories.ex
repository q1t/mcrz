defmodule His.App.ComplaintCategories do
  use Ecto.Schema

  schema "app_complaint_categories" do
    field :title, :string
    has_many :complaints, His.App.Complaints
    timestamps()
  end
end

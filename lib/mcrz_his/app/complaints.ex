defmodule His.App.Complaints do
  use Ecto.Schema

  schema "app_complaints" do
    field :body, :string
    belongs_to :complaint_categories, His.App.ComplaintCategories
    timestamps()
  end
end

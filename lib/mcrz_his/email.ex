defmodule His.Email do
  import Bamboo.Email
  import His.Web.Router.Helpers
  alias His.Web.Endpoint

  def verification(email, key) do
    base_email
    |> to(email)
    |> subject("МЦРЗ - подтверждение эл. почты")
    |> text_body("Для подтверждение почты перейдите по ссылке: "
               <> make_url(email_path(Endpoint, :verify, email: email, key: key)))
  end

  def password_reset(email, key) do
    base_email
    |> to(email)
    |> subject("МЦРЗ - восстановление пароля")
    |> text_body("Для восстановление пароля перейдите по ссылке: "
               <> make_url(password_path(Endpoint, :show, email: email, key: key)))
  end

  def verification_registration(user) do
    key = user.email_verification_key
    pw_key = user.password_reset_key
    path = email_path(Endpoint, :verify, email: user.email, key: key, pw_key: pw_key)
    base_email
    |> to(user.email)
    |> subject("МЦРЗ - регистрация")
    |> text_body("Для завершения регистрация пройдите по ссылке: "
               <> make_url(path))
  end

  def base_email do
    new_email
    |> from("noreply@mcrz.ru")
  end

  defp make_url(path) do
    Endpoint.url <> path
  end
end

defmodule His.Patients.Document do
  use Ecto.Schema
  import Ecto.{Query, Changeset}, warn: false

  schema "patient_documents" do
    field :agreed, :boolean, default: false
    field :local_path, :string
    field :name, :string
    field :template, :string
    field :file, :any, virtual: true
    belongs_to :patient, His.Patients.Patient

    timestamps()
  end

  def update_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:agreed, :name, :file])
    |> validate_required([:name])
    |> save_file()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:agreed, :name, :file])
    |> validate_required([:name, :file])
    |> save_file()
  end

  @root Path.join(File.cwd!(), "priv/uploads/patients")
  defp save_file(changeset) do
    file = get_change(changeset, :file)
    if file do
      ext = Path.extname(file.filename)
      new_path = Path.join(@root, random_filename() <> ext)
      File.cp!(file.path, new_path)
      put_change(changeset, :local_path, new_path)
    else
      changeset
    end
  end

  defp random_filename() do
    :crypto.strong_rand_bytes(32)
    |> Base.encode16(case: :lower)
  end
end

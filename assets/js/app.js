// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

// import socket from "./socket"
//

var multiple_inputs = $(".multiple-input")

for (var i = 0; i < multiple_inputs.length; i++) {
  apply_handler(multiple_inputs[i])
}

function apply_handler(parent_elem) {
  var [select] = $(parent_elem).children(":first")
  var [input] = $(parent_elem).children(":last")
  $(select).change(() => {
    var selected = select.options[select.selectedIndex].value
    var text = format($(input).val(), selected)
    $(input).val(text)
  })
}

function format(prev, n) {
  if (prev == "") {
    return n
  } else {
    return `${prev}, ${n}`
  }
}

// $(document).ready(function(){
//   $("input:radio:checked").data("chk",true);
//   let prev;
//   $("input:radio").click(function(){
//     $("input[name='"+$(this).attr("name")+"']:radio").not(this).removeData("chk");
//     $(this).data("chk",!$(this).data("chk"));
//     $(this).prop("checked",$(this).data("chk"));
//     let val = $(this).val() || $(this).data("prev")
//     if ($(this).data("chk")) {
//       $(this).data("prev", val);
//       console.log(val)
//       $(this).val(val);
//     } else {
//       $(this).val("no");
//       console.log("prev", val)
//       $(this).data("prev", val);
//     }
//   });
// });

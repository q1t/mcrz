defmodule His.Repo.Migrations.AddEmailVerificationFieldsToUser do
  use Ecto.Migration

  def change do
    alter table(:accounts_users) do
      add :verified, :boolean, default: false
      add :email_verification_key, :string
    end
  end
end

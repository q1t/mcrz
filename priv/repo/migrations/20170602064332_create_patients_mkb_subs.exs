defmodule His.Repo.Migrations.CreateHis.Patients.MkbSubs do
  use Ecto.Migration

  def change do
    create table(:patients_mkb_sub_codes) do
      add :code, :string
      add :short_desc, :string
      add :mkb_id, references(:patients_mkb_codes, on_delete: :delete_all), null: false

      timestamps()
    end

    create unique_index(:patients_mkb_sub_codes, [:code])
    create index(:patients_mkb_sub_codes, [:mkb_id])
  end
end

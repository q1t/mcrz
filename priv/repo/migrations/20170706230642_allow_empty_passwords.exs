defmodule His.Repo.Migrations.AllowEmptyPasswords do
  use Ecto.Migration

  def up do
    alter table(:accounts_users) do
      modify :password_hash, :string, null: true
    end
  end

  def down do
    alter table(:accounts_users) do
      modify :password_hash, :string, null: false, default: nil
    end
  end
end

defmodule His.Repo.Migrations.CreateHis.App.ComplaintCategories do
  use Ecto.Migration

  def change do
    create table(:app_complaint_categories) do
      add :title, :text
      timestamps()
    end
  end
end

defmodule His.Repo.Migrations.AddAbbrToVisitation do
  use Ecto.Migration

  def change do
    alter table(:patients_visitations) do
      add :so, :map
      add :ab, :map
      add :ds, :map
      add :sg, :map
      add :ou, :map
      add :zk, :map
      add :jb, :map
      add :po, :map
    end
  end
end

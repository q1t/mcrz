defmodule His.Repo.Migrations.CreateHis.Patients.Patient do
  use Ecto.Migration

  def change do
    create table(:patients_patients) do
      add :email, :string
      add :last_name, :string
      add :first_name, :string
      add :middle_name, :string
      add :code, :string
      add :address, :string
      add :phone_number, :string
      add :birthday, :date
      add :first_visit, :naive_datetime

      timestamps()
    end

  end
end

defmodule His.Repo.Migrations.CreateHis.Patient.Document do
  use Ecto.Migration

  def change do
    create table(:patient_documents) do
      add :local_path, :string
      add :name, :string
      add :agreed, :boolean, default: false, null: false
      add :template, :string
      add :patient_id, references(:patients_patients, on_delete: :delete_all)
      timestamps()
    end

  end
end

defmodule His.Repo.Migrations.AddRefToComplaints do
  use Ecto.Migration

  def change do
    alter table(:app_complaints) do
      add :complaint_categories_id, references(:app_complaint_categories, on_delete: :delete_all)
    end
  end
end

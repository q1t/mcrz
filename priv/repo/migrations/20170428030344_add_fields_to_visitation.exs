defmodule His.Repo.Migrations.AddFieldsToVisitation do
  use Ecto.Migration

  def change do
    alter table(:patients_visitations) do
      add :fields, :map
    end
  end
end

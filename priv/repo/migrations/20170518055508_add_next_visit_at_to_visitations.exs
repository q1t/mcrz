defmodule His.Repo.Migrations.AddNextVisitAtToVisitations do
  use Ecto.Migration

  def change do
    alter table(:patients_visitations) do
      add :next_visit_at, :utc_datetime
    end
  end
end

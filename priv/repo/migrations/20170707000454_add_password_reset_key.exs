defmodule His.Repo.Migrations.AddPasswordResetKey do
  use Ecto.Migration

  def change do
    alter table(:accounts_users) do
      add :password_reset_key, :string
    end
  end
end

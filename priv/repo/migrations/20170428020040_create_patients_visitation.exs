defmodule His.Repo.Migrations.CreateHis.Patients.Visitation do
  use Ecto.Migration

  def change do
    create table(:patients_visitations) do
      add :first, :boolean, default: false, null: false
      add :patient_id, references(:patients_patients, on_delete: :nothing)
      add :user_id, references(:accounts_users, on_delete: :nothing)

      timestamps()
    end

    create index(:patients_visitations, [:patient_id])
    create index(:patients_visitations, [:user_id])
  end
end

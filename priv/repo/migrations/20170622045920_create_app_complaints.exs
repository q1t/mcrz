defmodule His.Repo.Migrations.CreateHis.App.Complaints do
  use Ecto.Migration

  def change do
    create table(:app_complaints) do
      add :body, :text

      timestamps()
    end

  end
end

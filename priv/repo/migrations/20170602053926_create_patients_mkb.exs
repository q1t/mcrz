defmodule His.Repo.Migrations.CreateHis.Patients.Mkb do
  use Ecto.Migration

  def change do
    create table(:patients_mkb_codes) do
      add :code, :string
      add :short_desc, :string

      timestamps()
    end

    create unique_index(:patients_mkb_codes, [:code])
  end
end

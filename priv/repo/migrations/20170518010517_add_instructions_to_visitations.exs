defmodule His.Repo.Migrations.AddInstructionsToVisitations do
  use Ecto.Migration

  def change do
    alter table(:patients_visitations) do
      add :instructions, :map
    end
  end
end

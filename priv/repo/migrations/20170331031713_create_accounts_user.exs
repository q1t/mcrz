defmodule His.Repo.Migrations.CreateHis.Accounts.User do
  use Ecto.Migration

  def change do
    create table(:accounts_users) do
      add :email,         :string, null: false
      add :last_name,     :string, null: false
      add :first_name,    :string, null: false
      add :middle_name,   :string
      add :position,      :string, null: false
      add :password_hash, :string, null: false
      timestamps()
    end

    create constraint(:accounts_users, :allowed_positions,
      check: "position in ('doctor', 'admin', 'nurse', 'registrar')")

    create unique_index(:accounts_users, [:email])
  end
end

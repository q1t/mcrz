defmodule His.AppTest do
  use His.DataCase

  alias His.App
  alias His.App.Complaints

  @create_attrs %{}
  @update_attrs %{}
  @invalid_attrs %{}

  def fixture(:complaints, attrs \\ @create_attrs) do
    {:ok, complaints} = App.create_complaints(attrs)
    complaints
  end

  test "list_complaints/1 returns all complaints" do
    complaints = fixture(:complaints)
    assert App.list_complaints() == [complaints]
  end

  test "get_complaints! returns the complaints with given id" do
    complaints = fixture(:complaints)
    assert App.get_complaints!(complaints.id) == complaints
  end

  test "create_complaints/1 with valid data creates a complaints" do
    assert {:ok, %Complaints{} = complaints} = App.create_complaints(@create_attrs)
  end

  test "create_complaints/1 with invalid data returns error changeset" do
    assert {:error, %Ecto.Changeset{}} = App.create_complaints(@invalid_attrs)
  end

  test "update_complaints/2 with valid data updates the complaints" do
    complaints = fixture(:complaints)
    assert {:ok, complaints} = App.update_complaints(complaints, @update_attrs)
    assert %Complaints{} = complaints
  end

  test "update_complaints/2 with invalid data returns error changeset" do
    complaints = fixture(:complaints)
    assert {:error, %Ecto.Changeset{}} = App.update_complaints(complaints, @invalid_attrs)
    assert complaints == App.get_complaints!(complaints.id)
  end

  test "delete_complaints/1 deletes the complaints" do
    complaints = fixture(:complaints)
    assert {:ok, %Complaints{}} = App.delete_complaints(complaints)
    assert_raise Ecto.NoResultsError, fn -> App.get_complaints!(complaints.id) end
  end

  test "change_complaints/1 returns a complaints changeset" do
    complaints = fixture(:complaints)
    assert %Ecto.Changeset{} = App.change_complaints(complaints)
  end
end

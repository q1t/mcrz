defmodule His.PatientsTest do
  use His.DataCase

  alias His.Patients
  alias His.Patients.Patient

  @create_attrs %{address: "some address", birthday: ~D[2010-04-17], code: "some code", email: "some email", first_name: "some first_name", first_visit: ~N[2010-04-17 14:00:00.000000], last_name: "some last_name", middle_name: "some middle_name", phone_number: "some phone_number"}
  @update_attrs %{address: "some updated address", birthday: ~D[2011-05-18], code: "some updated code", email: "some updated email", first_name: "some updated first_name", first_visit: ~N[2011-05-18 15:01:01.000000], last_name: "some updated last_name", middle_name: "some updated middle_name", phone_number: "some updated phone_number"}
  @invalid_attrs %{address: nil, birthday: nil, code: nil, email: nil, first_name: nil, first_visit: nil, last_name: nil, middle_name: nil, phone_number: nil}

  def fixture(:patient, attrs \\ @create_attrs) do
    {:ok, patient} = Patients.create_patient(attrs)
    patient
  end

  test "list_patients/1 returns all patients" do
    patient = fixture(:patient)
    assert Patients.list_patients() == [patient]
  end

  test "get_patient! returns the patient with given id" do
    patient = fixture(:patient)
    assert Patients.get_patient!(patient.id) == patient
  end

  test "create_patient/1 with valid data creates a patient" do
    assert {:ok, %Patient{} = patient} = Patients.create_patient(@create_attrs)
    assert patient.address == "some address"
    assert patient.birthday == ~D[2010-04-17]
    assert patient.code == "some code"
    assert patient.email == "some email"
    assert patient.first_name == "some first_name"
    assert patient.first_visit == ~N[2010-04-17 14:00:00.000000]
    assert patient.last_name == "some last_name"
    assert patient.middle_name == "some middle_name"
    assert patient.phone_number == "some phone_number"
  end

  test "create_patient/1 with invalid data returns error changeset" do
    assert {:error, %Ecto.Changeset{}} = Patients.create_patient(@invalid_attrs)
  end

  test "update_patient/2 with valid data updates the patient" do
    patient = fixture(:patient)
    assert {:ok, patient} = Patients.update_patient(patient, @update_attrs)
    assert %Patient{} = patient
    assert patient.address == "some updated address"
    assert patient.birthday == ~D[2011-05-18]
    assert patient.code == "some updated code"
    assert patient.email == "some updated email"
    assert patient.first_name == "some updated first_name"
    assert patient.first_visit == ~N[2011-05-18 15:01:01.000000]
    assert patient.last_name == "some updated last_name"
    assert patient.middle_name == "some updated middle_name"
    assert patient.phone_number == "some updated phone_number"
  end

  test "update_patient/2 with invalid data returns error changeset" do
    patient = fixture(:patient)
    assert {:error, %Ecto.Changeset{}} = Patients.update_patient(patient, @invalid_attrs)
    assert patient == Patients.get_patient!(patient.id)
  end

  test "delete_patient/1 deletes the patient" do
    patient = fixture(:patient)
    assert {:ok, %Patient{}} = Patients.delete_patient(patient)
    assert_raise Ecto.NoResultsError, fn -> Patients.get_patient!(patient.id) end
  end

  test "change_patient/1 returns a patient changeset" do
    patient = fixture(:patient)
    assert %Ecto.Changeset{} = Patients.change_patient(patient)
  end
end

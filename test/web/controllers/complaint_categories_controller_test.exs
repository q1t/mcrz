defmodule His.Web.ComplaintCategoriesControllerTest do
  use His.Web.ConnCase

  alias His.App

  @create_attrs %{title: "some title"}
  @update_attrs %{title: "some updated title"}
  @invalid_attrs %{title: nil}

  def fixture(:complaint_categories) do
    {:ok, complaint_categories} = App.create_complaint_categories(@create_attrs)
    complaint_categories
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, complaint_categories_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing Complaint categories"
  end

  test "renders form for new complaint_categories", %{conn: conn} do
    conn = get conn, complaint_categories_path(conn, :new)
    assert html_response(conn, 200) =~ "New Complaint categories"
  end

  test "creates complaint_categories and redirects to show when data is valid", %{conn: conn} do
    conn = post conn, complaint_categories_path(conn, :create), complaint_categories: @create_attrs

    assert %{id: id} = redirected_params(conn)
    assert redirected_to(conn) == complaint_categories_path(conn, :show, id)

    conn = get conn, complaint_categories_path(conn, :show, id)
    assert html_response(conn, 200) =~ "Show Complaint categories"
  end

  test "does not create complaint_categories and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, complaint_categories_path(conn, :create), complaint_categories: @invalid_attrs
    assert html_response(conn, 200) =~ "New Complaint categories"
  end

  test "renders form for editing chosen complaint_categories", %{conn: conn} do
    complaint_categories = fixture(:complaint_categories)
    conn = get conn, complaint_categories_path(conn, :edit, complaint_categories)
    assert html_response(conn, 200) =~ "Edit Complaint categories"
  end

  test "updates chosen complaint_categories and redirects when data is valid", %{conn: conn} do
    complaint_categories = fixture(:complaint_categories)
    conn = put conn, complaint_categories_path(conn, :update, complaint_categories), complaint_categories: @update_attrs
    assert redirected_to(conn) == complaint_categories_path(conn, :show, complaint_categories)

    conn = get conn, complaint_categories_path(conn, :show, complaint_categories)
    assert html_response(conn, 200) =~ "some updated title"
  end

  test "does not update chosen complaint_categories and renders errors when data is invalid", %{conn: conn} do
    complaint_categories = fixture(:complaint_categories)
    conn = put conn, complaint_categories_path(conn, :update, complaint_categories), complaint_categories: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit Complaint categories"
  end

  test "deletes chosen complaint_categories", %{conn: conn} do
    complaint_categories = fixture(:complaint_categories)
    conn = delete conn, complaint_categories_path(conn, :delete, complaint_categories)
    assert redirected_to(conn) == complaint_categories_path(conn, :index)
    assert_error_sent 404, fn ->
      get conn, complaint_categories_path(conn, :show, complaint_categories)
    end
  end
end

defmodule His.Web.RegistrationControllerTest do
  use His.Web.ConnCase

  alias His.Accounts

  @create_attrs %{email: "some email", password: "some password"}
  @update_attrs %{email: "some updated email", password: "some updated password"}
  @invalid_attrs %{email: nil, password: nil}

  def fixture(:registration) do
    {:ok, registration} = Accounts.create_registration(@create_attrs)
    registration
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, registration_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing Registrations"
  end

  test "renders form for new registrations", %{conn: conn} do
    conn = get conn, registration_path(conn, :new)
    assert html_response(conn, 200) =~ "New Registration"
  end

  test "creates registration and redirects to show when data is valid", %{conn: conn} do
    conn = post conn, registration_path(conn, :create), registration: @create_attrs

    assert %{id: id} = redirected_params(conn)
    assert redirected_to(conn) == registration_path(conn, :show, id)

    conn = get conn, registration_path(conn, :show, id)
    assert html_response(conn, 200) =~ "Show Registration"
  end

  test "does not create registration and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, registration_path(conn, :create), registration: @invalid_attrs
    assert html_response(conn, 200) =~ "New Registration"
  end

  test "renders form for editing chosen registration", %{conn: conn} do
    registration = fixture(:registration)
    conn = get conn, registration_path(conn, :edit, registration)
    assert html_response(conn, 200) =~ "Edit Registration"
  end

  test "updates chosen registration and redirects when data is valid", %{conn: conn} do
    registration = fixture(:registration)
    conn = put conn, registration_path(conn, :update, registration), registration: @update_attrs
    assert redirected_to(conn) == registration_path(conn, :show, registration)

    conn = get conn, registration_path(conn, :show, registration)
    assert html_response(conn, 200) =~ "some updated email"
  end

  test "does not update chosen registration and renders errors when data is invalid", %{conn: conn} do
    registration = fixture(:registration)
    conn = put conn, registration_path(conn, :update, registration), registration: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit Registration"
  end

  test "deletes chosen registration", %{conn: conn} do
    registration = fixture(:registration)
    conn = delete conn, registration_path(conn, :delete, registration)
    assert redirected_to(conn) == registration_path(conn, :index)
    assert_error_sent 404, fn ->
      get conn, registration_path(conn, :show, registration)
    end
  end
end

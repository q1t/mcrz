defmodule His.Web.MkbControllerTest do
  use His.Web.ConnCase

  alias His.Patients

  @create_attrs %{code: "some code", short_desc: "some short_desc"}
  @update_attrs %{code: "some updated code", short_desc: "some updated short_desc"}
  @invalid_attrs %{code: nil, short_desc: nil}

  def fixture(:mkb) do
    {:ok, mkb} = Patients.create_mkb(@create_attrs)
    mkb
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, mkb_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing Patients"
  end

  test "renders form for new patients", %{conn: conn} do
    conn = get conn, mkb_path(conn, :new)
    assert html_response(conn, 200) =~ "New Mkb"
  end

  test "creates mkb and redirects to show when data is valid", %{conn: conn} do
    conn = post conn, mkb_path(conn, :create), mkb: @create_attrs

    assert %{id: id} = redirected_params(conn)
    assert redirected_to(conn) == mkb_path(conn, :show, id)

    conn = get conn, mkb_path(conn, :show, id)
    assert html_response(conn, 200) =~ "Show Mkb"
  end

  test "does not create mkb and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, mkb_path(conn, :create), mkb: @invalid_attrs
    assert html_response(conn, 200) =~ "New Mkb"
  end

  test "renders form for editing chosen mkb", %{conn: conn} do
    mkb = fixture(:mkb)
    conn = get conn, mkb_path(conn, :edit, mkb)
    assert html_response(conn, 200) =~ "Edit Mkb"
  end

  test "updates chosen mkb and redirects when data is valid", %{conn: conn} do
    mkb = fixture(:mkb)
    conn = put conn, mkb_path(conn, :update, mkb), mkb: @update_attrs
    assert redirected_to(conn) == mkb_path(conn, :show, mkb)

    conn = get conn, mkb_path(conn, :show, mkb)
    assert html_response(conn, 200) =~ "some updated code"
  end

  test "does not update chosen mkb and renders errors when data is invalid", %{conn: conn} do
    mkb = fixture(:mkb)
    conn = put conn, mkb_path(conn, :update, mkb), mkb: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit Mkb"
  end

  test "deletes chosen mkb", %{conn: conn} do
    mkb = fixture(:mkb)
    conn = delete conn, mkb_path(conn, :delete, mkb)
    assert redirected_to(conn) == mkb_path(conn, :index)
    assert_error_sent 404, fn ->
      get conn, mkb_path(conn, :show, mkb)
    end
  end
end

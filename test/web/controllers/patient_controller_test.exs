defmodule His.Web.PatientControllerTest do
  use His.Web.ConnCase

  alias His.Patients

  @create_attrs %{address: "some address", birthday: ~D[2010-04-17], code: "some code", email: "some email", first_name: "some first_name", first_visit: ~N[2010-04-17 14:00:00.000000], last_name: "some last_name", middle_name: "some middle_name", phone_number: "some phone_number"}
  @update_attrs %{address: "some updated address", birthday: ~D[2011-05-18], code: "some updated code", email: "some updated email", first_name: "some updated first_name", first_visit: ~N[2011-05-18 15:01:01.000000], last_name: "some updated last_name", middle_name: "some updated middle_name", phone_number: "some updated phone_number"}
  @invalid_attrs %{address: nil, birthday: nil, code: nil, email: nil, first_name: nil, first_visit: nil, last_name: nil, middle_name: nil, phone_number: nil}

  def fixture(:patient) do
    {:ok, patient} = Patients.create_patient(@create_attrs)
    patient
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, patient_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing Patients"
  end

  test "renders form for new patients", %{conn: conn} do
    conn = get conn, patient_path(conn, :new)
    assert html_response(conn, 200) =~ "New Patient"
  end

  test "creates patient and redirects to show when data is valid", %{conn: conn} do
    conn = post conn, patient_path(conn, :create), patient: @create_attrs

    assert %{id: id} = redirected_params(conn)
    assert redirected_to(conn) == patient_path(conn, :show, id)

    conn = get conn, patient_path(conn, :show, id)
    assert html_response(conn, 200) =~ "Show Patient"
  end

  test "does not create patient and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, patient_path(conn, :create), patient: @invalid_attrs
    assert html_response(conn, 200) =~ "New Patient"
  end

  test "renders form for editing chosen patient", %{conn: conn} do
    patient = fixture(:patient)
    conn = get conn, patient_path(conn, :edit, patient)
    assert html_response(conn, 200) =~ "Edit Patient"
  end

  test "updates chosen patient and redirects when data is valid", %{conn: conn} do
    patient = fixture(:patient)
    conn = put conn, patient_path(conn, :update, patient), patient: @update_attrs
    assert redirected_to(conn) == patient_path(conn, :show, patient)

    conn = get conn, patient_path(conn, :show, patient)
    assert html_response(conn, 200) =~ "some updated address"
  end

  test "does not update chosen patient and renders errors when data is invalid", %{conn: conn} do
    patient = fixture(:patient)
    conn = put conn, patient_path(conn, :update, patient), patient: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit Patient"
  end

  test "deletes chosen patient", %{conn: conn} do
    patient = fixture(:patient)
    conn = delete conn, patient_path(conn, :delete, patient)
    assert redirected_to(conn) == patient_path(conn, :index)
    assert_error_sent 404, fn ->
      get conn, patient_path(conn, :show, patient)
    end
  end
end

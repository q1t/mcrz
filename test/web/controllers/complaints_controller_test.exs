defmodule His.Web.ComplaintsControllerTest do
  use His.Web.ConnCase

  alias His.App

  @create_attrs %{body: "some body"}
  @update_attrs %{body: "some updated body"}
  @invalid_attrs %{body: nil}

  def fixture(:complaints) do
    {:ok, complaints} = App.create_complaints(@create_attrs)
    complaints
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, complaints_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing Complaints"
  end

  test "renders form for new complaints", %{conn: conn} do
    conn = get conn, complaints_path(conn, :new)
    assert html_response(conn, 200) =~ "New Complaints"
  end

  test "creates complaints and redirects to show when data is valid", %{conn: conn} do
    conn = post conn, complaints_path(conn, :create), complaints: @create_attrs

    assert %{id: id} = redirected_params(conn)
    assert redirected_to(conn) == complaints_path(conn, :show, id)

    conn = get conn, complaints_path(conn, :show, id)
    assert html_response(conn, 200) =~ "Show Complaints"
  end

  test "does not create complaints and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, complaints_path(conn, :create), complaints: @invalid_attrs
    assert html_response(conn, 200) =~ "New Complaints"
  end

  test "renders form for editing chosen complaints", %{conn: conn} do
    complaints = fixture(:complaints)
    conn = get conn, complaints_path(conn, :edit, complaints)
    assert html_response(conn, 200) =~ "Edit Complaints"
  end

  test "updates chosen complaints and redirects when data is valid", %{conn: conn} do
    complaints = fixture(:complaints)
    conn = put conn, complaints_path(conn, :update, complaints), complaints: @update_attrs
    assert redirected_to(conn) == complaints_path(conn, :show, complaints)

    conn = get conn, complaints_path(conn, :show, complaints)
    assert html_response(conn, 200) =~ "some updated body"
  end

  test "does not update chosen complaints and renders errors when data is invalid", %{conn: conn} do
    complaints = fixture(:complaints)
    conn = put conn, complaints_path(conn, :update, complaints), complaints: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit Complaints"
  end

  test "deletes chosen complaints", %{conn: conn} do
    complaints = fixture(:complaints)
    conn = delete conn, complaints_path(conn, :delete, complaints)
    assert redirected_to(conn) == complaints_path(conn, :index)
    assert_error_sent 404, fn ->
      get conn, complaints_path(conn, :show, complaints)
    end
  end
end

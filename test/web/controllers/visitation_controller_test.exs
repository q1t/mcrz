defmodule His.Web.VisitationControllerTest do
  use His.Web.ConnCase

  alias His.Patients

  @create_attrs %{first: true}
  @update_attrs %{first: false}
  @invalid_attrs %{first: nil}

  def fixture(:visitation) do
    {:ok, visitation} = Patients.create_visitation(@create_attrs)
    visitation
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, patient_visitation_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing Visitations"
  end

  test "renders form for new visitations", %{conn: conn} do
    conn = get conn, patient_visitation_path(conn, :new)
    assert html_response(conn, 200) =~ "New Visitation"
  end

  test "creates visitation and redirects to show when data is valid", %{conn: conn} do
    conn = post conn, patient_visitation_path(conn, :create), visitation: @create_attrs

    assert %{id: id} = redirected_params(conn)
    assert redirected_to(conn) == patient_visitation_path(conn, :show, id)

    conn = get conn, patient_visitation_path(conn, :show, id)
    assert html_response(conn, 200) =~ "Show Visitation"
  end

  test "does not create visitation and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, patient_visitation_path(conn, :create), visitation: @invalid_attrs
    assert html_response(conn, 200) =~ "New Visitation"
  end

  test "renders form for editing chosen visitation", %{conn: conn} do
    visitation = fixture(:visitation)
    conn = get conn, patient_visitation_path(conn, :edit, visitation)
    assert html_response(conn, 200) =~ "Edit Visitation"
  end

  test "updates chosen visitation and redirects when data is valid", %{conn: conn} do
    visitation = fixture(:visitation)
    conn = put conn, patient_visitation_path(conn, :update, visitation), visitation: @update_attrs
    assert redirected_to(conn) == patient_visitation_path(conn, :show, visitation)

    conn = get conn, patient_visitation_path(conn, :show, visitation)
    assert html_response(conn, 200)
  end

  test "does not update chosen visitation and renders errors when data is invalid", %{conn: conn} do
    visitation = fixture(:visitation)
    conn = put conn, patient_visitation_path(conn, :update, visitation), visitation: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit Visitation"
  end

  test "deletes chosen visitation", %{conn: conn} do
    visitation = fixture(:visitation)
    conn = delete conn, patient_visitation_path(conn, :delete, visitation)
    assert redirected_to(conn) == patient_visitation_path(conn, :index)
    assert_error_sent 404, fn ->
      get conn, patient_visitation_path(conn, :show, visitation)
    end
  end
end

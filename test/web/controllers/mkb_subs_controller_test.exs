defmodule His.Web.MkbSubsControllerTest do
  use His.Web.ConnCase

  alias His.Patients

  @create_attrs %{code: "some code", short_desc: "some short_desc"}
  @update_attrs %{code: "some updated code", short_desc: "some updated short_desc"}
  @invalid_attrs %{code: nil, short_desc: nil}

  def fixture(:mkb_subs) do
    {:ok, mkb_subs} = Patients.create_mkb_subs(@create_attrs)
    mkb_subs
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, mkb_subs_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing Mkb subs"
  end

  test "renders form for new mkb_subs", %{conn: conn} do
    conn = get conn, mkb_subs_path(conn, :new)
    assert html_response(conn, 200) =~ "New Mkb subs"
  end

  test "creates mkb_subs and redirects to show when data is valid", %{conn: conn} do
    conn = post conn, mkb_subs_path(conn, :create), mkb_subs: @create_attrs

    assert %{id: id} = redirected_params(conn)
    assert redirected_to(conn) == mkb_subs_path(conn, :show, id)

    conn = get conn, mkb_subs_path(conn, :show, id)
    assert html_response(conn, 200) =~ "Show Mkb subs"
  end

  test "does not create mkb_subs and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, mkb_subs_path(conn, :create), mkb_subs: @invalid_attrs
    assert html_response(conn, 200) =~ "New Mkb subs"
  end

  test "renders form for editing chosen mkb_subs", %{conn: conn} do
    mkb_subs = fixture(:mkb_subs)
    conn = get conn, mkb_subs_path(conn, :edit, mkb_subs)
    assert html_response(conn, 200) =~ "Edit Mkb subs"
  end

  test "updates chosen mkb_subs and redirects when data is valid", %{conn: conn} do
    mkb_subs = fixture(:mkb_subs)
    conn = put conn, mkb_subs_path(conn, :update, mkb_subs), mkb_subs: @update_attrs
    assert redirected_to(conn) == mkb_subs_path(conn, :show, mkb_subs)

    conn = get conn, mkb_subs_path(conn, :show, mkb_subs)
    assert html_response(conn, 200) =~ "some updated code"
  end

  test "does not update chosen mkb_subs and renders errors when data is invalid", %{conn: conn} do
    mkb_subs = fixture(:mkb_subs)
    conn = put conn, mkb_subs_path(conn, :update, mkb_subs), mkb_subs: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit Mkb subs"
  end

  test "deletes chosen mkb_subs", %{conn: conn} do
    mkb_subs = fixture(:mkb_subs)
    conn = delete conn, mkb_subs_path(conn, :delete, mkb_subs)
    assert redirected_to(conn) == mkb_subs_path(conn, :index)
    assert_error_sent 404, fn ->
      get conn, mkb_subs_path(conn, :show, mkb_subs)
    end
  end
end

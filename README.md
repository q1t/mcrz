### Зависимости
1. `postgresql >= 9.5`
2. [`Elixir >= 1.4`](http://elixir-lang.org)
3. если вы не используете erlang/elixir релизы, то на сервере должно быть установлено `Erlang/OTP >= 19` и `Elixir >= 1.4`

### Конфигурация 

Конфигурация для окружений `dev`, `prod`, `test` находится в `$APP/config/<env>.exs`, где вы сможете настроить порты и SSL (возможно также использование reverse proxy таких как nginx)

### Сборка

Рекомендованный сопособ деплоя Elixir/Phoenix приложения - Release (релизы), почитать больше можно по этой [ссылке](https://github.com/bitwalker/distillery)

Также возможно просто запустить на сервере `MIX_ENV=prod mix phx.server --no-halt` (не рекомендуется, больше информации в [phoenix гайде](https://hexdocs.pm/phoenix/deployment.html))

#### Для разработки

1. Склонировать репозиторий `git clone https://gitlab.com/q1t/mcrz.git`
2. перейти в директорию `mcrz`
3. запустить `mix do deps.get, compile`
4. зайти в `$APP/assets` и установить node зависимости `npm i`
5. из корня приложения запустить `mix ecto.create`, `mix ecto.migrate` для того, чтобы создать и запустить миграции БД. (для этого вам понадобится пользователь из пункта **Использование дампа БД**)
6. после этого можно запустить Elixir Interactive Shell (iex) c приложением `iex -S mix phx.server`

#### Для деплоя

1. Склонировать репозиторий `git clone https://gitlab.com/q1t/mcrz.git`
2. перейти в директорию `mcrz`
3. запустить `MIX_ENV=prod mix do deps.get, compile`
4. зайти в `$APP/assets` и установить node зависимости `npm i`
5. запустить `npm run deploy`
6. из корня приложения запустить `mix ecto.create`, `mix ecto.migrate` для того, чтобы создать и запустить миграции БД, а также `MIX_ENV=prod mix phx.digest`. (для этого вам понадобится пользователь из пункта **Использование дампа БД**)
7. После этого собрать релиз или [запустить приложениe](#для-разработки)

#### Использование дампа БД

1. Eсли вам нужны данные которые были созданы в процессе разработки (некоторое кол-во записей МКБ и Жалоб), можно использовать утилиту pgsql, пример - `pgsql -U <username> <database_name> < mcrz_dev.dump.sql` (будет выслан по почте)
1.1  `username` и `database_name` вы можете найти или отредактировать в `config/<env>.exs`
1.2 перед этим пользователь с `username` и правами на БД должен быть создан. Прим.

```
CREATE USER postgres;
ALTER USER postgres PASSWORD 'postgres';
CREATE DATABASE hexpm_dev;
GRANT ALL PRIVILEGES ON DATABASE hexpm_dev TO postgres;
ALTER USER postgres WITH SUPERUSER;
```

2. Eсли вам нужна только структура и нет возможности использовать миграции, то запустить из корня приложения `mix ecto.load -d /path/to/structure.sql` (будет выслан по почте)

#### Использование _секретов (таких как api ключи и пользователь и пароль к БД)_ в `prod` окружении

В продакшн окружении phoenix конфиг будет ожидать наличие файла `config/prod.secret.exs` c указанными данными, примерно как в `dev.exs`.

#### Настройка почтового сервиса

В данном приложении используется библиотека [bamboo](https://github.com/thoughtbot/bamboo), обратитесь к списку провайдеров и их настройки в докуменатции к bamboo.
